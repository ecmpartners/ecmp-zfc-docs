The ZFC fully integrates in the IBM FileNet P8 architecture.
It stores de configuration information in the data layer and operates in the business logic layer.
The scripting engine has a connection interface which can connect to code libraries, components or communication protocols.


![Application Service overview](img/application_architecture/baseline_architecture.jpg)
