The _ECMP Zero Footprint Client_ office integration can also be used from a custom web application. The document can be opened in the corresponding Microsoft Office application by navigating to a special URL. The custom web application is responsible for the creation of the correct URL. You can find the structure of this at URL at [https://docs.microsoft.com/office/client-developer/office-uri-schemes](https://docs.microsoft.com/office/client-developer/office-uri-schemes). The URL must match the configuration of the _ECMP ZFC Server_. This diagram shows how the different elements should be combined together to build a valid URL.

![path](img/office_url.svg)

The URL contains the following elements:

* The `scheme-name` is coupled to the type of the document you want to open. For Microsoft Word documents the value is `ms-word`, for Excel documents `ms-excel` and for PowerPoint you should use the value `ms-powerpoint`. Check the Microsoft documentation if you want to support other file types.

* The `server-url` and the optional `path-prefix` should correspond to the values specified in the [application configuration](../installation/configuration_plugin_installation.md) in the _General Configuration_ section.

* The `document-path` has the following structure:

![document-path](img/document_path.svg)

For the document path there are a few rules you have to follow:

* If the object store is hidden then this path should not include the name of the object store.
* If the document title does not end with a file extension then an extension corresponding to the file type should be appended to the document title, e.g. `.docx` for Microsoft Word documents.
* The path may optionally contain extra information in the `token` section. Note that this information may also contain extra forward slashes.

If you have constructed a valid path, then the simplest way to test this is to construct a simple href-link in your webpage:

```html
<a href="ms-word:https://www.example.com/zfc/OS/editdocument/{80CFF080-0000-CF1D-931E-1AAE33E32773}/file-sample.docx">file-sample.docx</a>
```

This will open the document in Microsoft Word. The application will handle the authentication of the user.

You can also use JavaScript to open the Office document from a webpage. This function will open the URL supplied as the argument:
```JavaScript
function openURI(uri) {
    function createIframe(){
        var iframe = document.createElement("iframe");
        iframe.style.visibility = 'hidden';
        iframe.style.display = 'none';
        iframe.src = uri;
        document.body.appendChild(iframe);
    }

    setTimeout(createIframe, 5);
}
```