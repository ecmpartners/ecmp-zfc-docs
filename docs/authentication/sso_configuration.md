Make sure you know the way you want to implement SSO by examining the [SSO Authentication](sso_authentication.md) guide.

## Enable SSO in the ZFC server configuration

* In the administration desktop of Content Navigator goto the the plug-in settings of the __ECMP Zero Footprint Client Configuration Plugin__
* Enable Single Sign-on in the Single Sign-on Configuration section.
* Supply a value for the __Logon Path Prefix__. This should be a path that triggers the authentication sequence for your external authentication provider.
* Supply a value for the __Logon Message__. This is the title of the URL file shown in Window Explorer prompting the user to logon to the application
* Save your settings

## Configure the TAI

* Copy the jar-file containing the TAI to the folder ```AppServer/lib/ext``` folder of your WebSphere installation.
* Navigate to __Security > Global Security__ page.
* In the __Authentication > Web and SIP security__ section select __Trust association__.
* Select __New...__ in the  __Additional Properties > Interceptors__ page.

* In the wizard page use for the __Interceptor class name__ the value ```nl.ecmpartners.zfc.server.tai.ZfcTrustAssociationInterceptor``` 
* Add the following custom properties:

| Input | Value  |
| ------|--------|
|zfcPathPrefix| The path prefix specified in the application configuration |
|logonPathPrefix| The logon path prefix specified in the application configuration |
|baseUrl| The logon path prefix specified in the application configuration |
|skipCookies|The names of the cookies separated by a semicolon for which the TAI should skip further processing. Typically these are the cookies set by you external authentication provider. You should provide another TAI to handle these cookies|
|skipHeaders|The names of the headers separated by a semicolon for which the TAI should skip further processing. Typically these are the headers set by you external authentication provider. You should provide another TAI to handle these headers|

* Restart WebSphere!
* Check the WebSphere log for error messages.
* A successful deploy should give the following messages in the WebSphere log-file:

```
00000001 TrustAssociat A   SECJ0121I: Trust Association Init class nl.ecmpartners.zfc.server.tai.ZfcTrustAssociationInterceptor
``` 
