The user must first be authenticated before the user can have acces to the object store. The default behavior of the application is to use Basic Authentication for user authentication. In that case a Windows security dialog is shown, prompting the user for a user name and a password:

![Windows Security Prompt](img/windows_security_prompt.png)

These credentials are send to the server using Base64 encoding. The WebSphere application server is responsible for the processing of these credential. The identity of the user is then passed to the _ZFC Server_ application. In this diagram the details of this mechanisme is shown:

![Basic Authentication Sequence Diagram](diagrams/office_basic_auth.svg)

Note that in this flow the `OPTIONS` request does not require authentication, whereas all other requests require an authenticated user.