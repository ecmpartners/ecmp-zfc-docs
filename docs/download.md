---
password: downloadzfc2025
---
# Downloads

On this page you can download the different versions of the software.

## Version 1.6.3 (latest version)

The object store feature add-on: [ECMPZeroFootprintExtension-1.0.0.zip](./download/head/ECMPZeroFootprintExtension-1.0.0.zip)

The _ECMP Zero Footprint Client Configuration_ plug-in: [ecmp-configuration-plugin.jar](./download/head/ecmp-configuration-plugin.jar)

The _ECMP Zero Footprint Office Integration_ plug-in: [ecmp-office-plugin.jar](./download/head/ecmp-office-plugin.jar)

The _ECMP ZFC Server_: [ecmp-webdav-server.ear](./download/head/ecmp-webdav-server.ear)

The _ECMP ZFC Server_ (just the war-file): [ecmp-webdav-server.war](./download/head/ecmp-webdav-server.war)

## Version 1.6.2

The object store feature add-on: [ECMPZeroFootprintExtension-1.0.0.zip](./download/1.6.2/ECMPZeroFootprintExtension-1.0.0.zip)

The _ECMP Zero Footprint Client Configuration_ plug-in: [ecmp-configuration-plugin.jar](./download/1.6.2/ecmp-configuration-plugin.jar)

The _ECMP Zero Footprint Office Integration_ plug-in: [ecmp-office-plugin.jar](./download/1.6.2/ecmp-office-plugin.jar)

The _ECMP ZFC Server_: [ecmp-webdav-server.ear](./download/1.6.2/ecmp-webdav-server.ear)

The _ECMP ZFC Server_ (just the war-file): [ecmp-webdav-server.war](./download/1.6.2/ecmp-webdav-server.war)

## Version 1.6.1

The object store feature add-on: [ECMPZeroFootprintExtension-1.0.0.zip](./download/1.6.1//ECMPZeroFootprintExtension-1.0.0.zip)

The _ECMP Zero Footprint Client Configuration_ plug-in: [ecmp-configuration-plugin.jar](./download/1.6.1/ecmp-configuration-plugin.jar)

The _ECMP Zero Footprint Office Integration_ plug-in: [ecmp-office-plugin.jar](./download/1.6.1/ecmp-configuration-plugin.jarecmp-office-plugin.jar)

The _ECMP ZFC Server_: [ecmp-webdav-server.ear](./download/1.6.1/ecmp-webdav-server.ear)

The _ECMP ZFC Server_ (just the war-file): [ecmp-webdav-server.war](./download/1.6.1/ecmp-webdav-server.war)

## Version 1.6

The object store feature add-on: [ECMPZeroFootprintExtension-1.0.0.zip](./download/1.6.0/ECMPZeroFootprintExtension-1.0.0.zip)

The _ECMP Zero Footprint Client Configuration_ plug-in: [ecmp-configuration-plugin.jar](./download/1.6.0/ecmp-configuration-plugin.jar)

The _ECMP Zero Footprint Office Integration_ plug-in: [ecmp-office-plugin.jar](./download/1.6.0/ecmp-office-plugin.jar)

The _ECMP ZFC Server_: [ecmp-webdav-server.ear](./download/1.6.0/ecmp-webdav-server.ear)

The _ECMP ZFC Server_ (just the war-file): [ecmp-webdav-server.war](./download/1.6.0/ecmp-webdav-server.war)

## Version 1.5.

The object store feature add-on: [ECMPZeroFootprintExtension-1.0.0.zip](./download/1.5.0/ECMPZeroFootprintExtension-1.0.0.zip)

The _ECMP Zero Footprint Client Configuration_ plug-in: [ecmp-configuration-plugin.jar](./download/1.5.0/ecmp-configuration-plugin.jar)

The _ECMP Zero Footprint Office Integration_ plug-in: [ecmp-office-plugin.jar](./download/1.5.0/ecmp-office-plugin.jar)

The _ECMP ZFC Server_: [ecmp-webdav-server.ear](./download/1.5.0/ecmp-webdav-server.ear)

The _ECMP ZFC Server_ (just the war-file): [ecmp-webdav-server.war](./download/1.5.0/ecmp-webdav-server.war)

## Version 1.4.1

The object store feature add-on: [ECMPZeroFootprintExtension-1.0.0.zip](./download/1.4.1/ECMPZeroFootprintExtension-1.0.0.zip)

The _ECMP Zero Footprint Client Configuration_ plug-in: [ecmp-configuration-plugin.jar](./download/1.4.1/ecmp-configuration-plugin.jar)

The _ECMP Zero Footprint Office Integration_ plug-in: [ecmp-office-plugin.jar](./download/1.4.1/ecmp-office-plugin.jar)

The _ECMP ZFC Server_: [ecmp-webdav-server.ear](./download/1.4.1/ecmp-webdav-server.ear)
