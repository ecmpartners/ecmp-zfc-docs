# Recycle bins for folders

When Windows Explorer deletes a folder it will first delete all the files contained in the folder and when the folder is empty the actual folder will be deleted. You also don't know in advance if the user is just delete a single document of a folder containing the document. This imposes a problem when you want to want to implement a recycle bin which also includes folders, while preserving the existing hierarchy of documents and folders. When the delete operation of the folder is executed the documents in the folders and the subfolders are already deleted. This problem can be solved using the [scripting cache](../configuration/server_scripts.md#caching-data) in combination with custom delete scripts for documents and folders.

You can use the following script as a template for deleting documents:

```javascript
importClass(Packages.com.filenet.api.core.Factory);
importClass(Packages.com.filenet.api.constants.PropertyNames);
importClass(Packages.com.filenet.api.constants.RefreshMode);
importClass(Packages.com.filenet.api.property.PropertyFilter);
importClass(Packages.com.filenet.api.constants.ClassNames);
importClass(Packages.com.filenet.api.util.Id);
importClass(Packages.com.filenet.api.constants.AutoUniqueName);
importClass(Packages.com.filenet.api.constants.DefineSecurityParentage);

importPackage(Packages.com.filenet.api.query);

importClass(java.lang.System);

var cache = Cache.getInstance();

/**
 * This method is called when a engine object is about to be deleted. This can
 * be a folder or a document. At this point no delete actions are performed.
 * When the object is a document then the version series object is already
 * fetched.
 * 
 * If this method returns false then the object should <strong>not</strong> be
 * deleted by the application. This can be used to implement an alternative
 * deletion strategy, like for example a recycle bin. In that case it is the
 * responsibility of the script to save the object.
 * 
 * @param engineObject
 *            the engine object
 * @param path 
 *            the path of the engine object
 * @return boolean indicating if the object should be deleted.
 */
function onDeleteObject(document, path) {

    if (path.indexOf("Prullenbak") > 0 ) {
        return true;        
    }
    
    // Prevent the default delete action

    var versionSeries = document.get_VersionSeries();
    versionSeries.clearPendingActions();

    // Fetch the current document containment relation
    
    var relation = getContainmentRelation(document, path.substring(path.indexOf("DESIGNOS") + 8, path.lastIndexOf("/")));
    if ( !relation ) {
        return false;
    }

    // Move document to new location

    moveDocument(document, relation);
    
    return true;
}

function getContainmentRelation(document, path) {
    
	var searchSQL = new SearchSQL();
		
	searchSQL.setFromClauseInitialValue(ClassNames.REFERENTIAL_CONTAINMENT_RELATIONSHIP, "R", true);
	searchSQL.setSelectList(PropertyNames.THIS + "," + PropertyNames.HEAD + "," + PropertyNames.TAIL + "," + PropertyNames.CONTAINMENT_NAME);
	searchSQL.setWhereClause( "Head = OBJECT('" + document.get_Id() + "') AND Tail = OBJECT('" + path  + "')" );
		
	System.out.println(searchSQL.toString());
	var searchScope = new SearchScope(document.getObjectStore());
	var objects = searchScope.fetchObjects(searchSQL, null, null, null);
	if (objects.isEmpty() ) {
	    System.out.println("Relation not found");
		return null;
	}
		
	return objects.iterator().next();
}

function getUsername(objectStore) {

    var connection = objectStore.getConnection();
    var filter = new PropertyFilter();
	filter.addIncludeProperty(0, null, null, PropertyNames.SHORT_NAME, null);
    var currentUser = Factory.User.fetchCurrent(connection, filter);
    
    System.out.println("Current user: " + currentUser.getProperties().getStringValue(PropertyNames.SHORT_NAME) );
    
    return currentUser.getProperties().getStringValue(PropertyNames.SHORT_NAME);
}

function moveDocument(document, oldRelation) {

    var objectStore = document.getObjectStore();
    var userPath = "/ECMP Zero Footprint Client Test/" + getUsername(objectStore) + "/Prullenbak";
    System.out.println("User Path: " + userPath );
    var targetFolder = Factory.Folder.fetchInstance(document.objectStore, userPath, null);

    var newRelation = targetFolder.file(document, AutoUniqueName.AUTO_UNIQUE, oldRelation.get_ContainmentName(), DefineSecurityParentage.DEFINE_SECURITY_PARENTAGE);
    newRelation.save(RefreshMode.REFRESH);

    oldRelation.delete();
    oldRelation.save(RefreshMode.NO_REFRESH);
    
    cacheDocumentRelation(oldRelation, newRelation);
}

function cacheDocumentRelation(oldRelation, newRelation) {

    var parentDocumentsKey = getDocumentsKey(oldRelation.get_Tail());
    var values = (cache.get(parentDocumentsKey) || []);
    values.push(newRelation.getObjectReference());
    cache.put(parentDocumentsKey, values);
}

function getDocumentsKey(parentFolder) {
    return parentFolder.getObjectReference().getObjectIdentity() + "_documents";
}
```
At the top of the method the scripting cache is fetched, playing a crucial role in the recycle bin scripts.

When a document is deleted the document is moved to the root folder of the recycle bin with the method `#!js moveDocument()`. However, the relation with the original parent folder is cached in an array with the method `#!js cacheDocumentRelation()`. This array contains all the deleted documents in the original parent folder.

You can use the following script as a template for handling the deletion of folders:

``` javascript
importClass(java.lang.System);
importClass(Packages.com.filenet.api.core.Factory);
importClass(Packages.com.filenet.api.util.Id);
importClass(Packages.com.filenet.api.constants.RefreshMode);
importClass(Packages.com.filenet.api.constants.PropertyNames);
importClass(Packages.com.filenet.api.property.PropertyFilter);
importClass(Packages.com.filenet.api.constants.ClassNames);
importClass(Packages.com.filenet.api.query.SearchSQL);
importClass(Packages.com.filenet.api.query.SearchScope);


var cache = Cache.getInstance();

/**
 * This method is called when a engine object is about to be deleted. This can
 * be a folder or a document. At this point no delete actions are performed.
 * When the object is a document then the version series object is already
 * fetched.
 * 
 * If this method returns false then the object should <strong>not</strong> be
 * deleted by the application. This can be used to implement an alternative
 * deletion strategy, like for example a recycle bin. In that case it is the
 * responsibility of the script to save the object.
 * 
 * @param engineObject
 *            the engine object
 * @param path 
 *            the path of the engine object
 * @return boolean indicating if the object should be deleted.
 */
function onDeleteObject(engineObject, path) {

    if (path.indexOf("Prullenbak") > 0 ) {
        return true;        
    }

    moveFolder(engineObject, getRecycleBinFolder(engineObject.objectStore) );
	return true;
}

function moveFolder(sourceFolder, destinationFolder) {

    sourceFolder.refresh(["Parent", "ClbTeamspace"]);

    cacheFolderParent(sourceFolder);

	sourceFolder.clearPendingActions();
	sourceFolder.set_Parent(destinationFolder);
	var teamSpace = sourceFolder.getProperties().getObjectValue("ClbTeamspace");
	sourceFolder.getProperties().putObjectValue("ClbTeamspace", null);
    ensureUniqueName(sourceFolder,destinationFolder);
	sourceFolder.save(RefreshMode.REFRESH);
    
    var childFolders = cache.get(getFoldersKey(sourceFolder));
    if ( childFolders ) {
        System.out.println( childFolders.length + " child folders");
        childFolders.forEach(reference => restorePreviousChildFolder(reference,sourceFolder));
    }
    
    var childDocuments = cache.get(getDocumentsKey(sourceFolder));
    if ( childDocuments ) {
        System.out.println( childDocuments.length + " child documents");
        childDocuments.forEach(reference => restorePreviousChildDocuments(reference,sourceFolder));
    }
    
	sourceFolder.getProperties().putObjectValue("ClbTeamspace", teamSpace);
	sourceFolder.save(RefreshMode.NO_REFRESH);
}

function ensureUniqueName(sourceFolder, destinationFolder) {

    sourceFolder.fetchProperties( [ "FolderName" ] )
    var folderName = sourceFolder.get_FolderName();

    if ( !isUniqueName(folderName, destinationFolder) ) {

        cache.put(getFolderRenamedKey(sourceFolder), folderName);

        var index = 0;
        do {
            index = index+1;
        } while (!isUniqueName(folderName + " (" + index + ")", destinationFolder) );

        sourceFolder.set_FolderName( folderName + " (" + index + ")");
    }
}

function isUniqueName(name, parentFolder) {

    var searchSQL = new SearchSQL();

    searchSQL.setFromClauseInitialValue("Folder", "F", true);
    searchSQL.setSelectList("Id");
    searchSQL.setWhereClause( "Parent = OBJECT('" + parentFolder.getObjectReference().getObjectIdentity() + "') AND FolderName = '" + name  + "'" );

    var searchScope = new SearchScope(parentFolder.getObjectStore());
    var objects = searchScope.fetchObjects(searchSQL, null, null, null);
    return objects.isEmpty();
}

function restorePreviousChildFolder(folderReference, parentFolder) {
    var childFolder = Factory.Folder.fetchInstance(parentFolder.getObjectStore(), 
        new Id(folderReference.getObjectIdentity()), null );

	var teamSpace = childFolder.getProperties().getObjectValue("ClbTeamspace");
	childFolder.getProperties().putObjectValue("ClbTeamspace", null);
	
    if ( cache.get(getFolderRenamedKey(childFolder) ) ) {
        childFolder.set_FolderName(cache.get(getFolderRenamedKey(childFolder)));
    }

    childFolder.set_Parent(parentFolder);
    childFolder.save(RefreshMode.NO_REFRESH);

	childFolder.getProperties().putObjectValue("ClbTeamspace", teamSpace);
	childFolder.save(RefreshMode.NO_REFRESH);
}

function restorePreviousChildDocuments(relationReference, parentFolder) {
    var relation = Factory.ReferentialContainmentRelationship.getInstance(parentFolder.getObjectStore(), 
        relationReference.getClassIdentity(), new Id(relationReference.getObjectIdentity()) );
    relation.set_Tail(parentFolder);
    relation.save(RefreshMode.NO_REFRESH);
}

function getFoldersKey(folder) {
    return folder.getObjectReference().getObjectIdentity() + "_folders";
}

function getDocumentsKey(folder) {
    return folder.getObjectReference().getObjectIdentity() + "_documents";
}

function getFolderRenamedKey(folder) {
    return folder.getObjectReference().getObjectIdentity() + "_renamed";
}

function cacheFolderParent(folder) {
    var parentFoldersKey = getFoldersKey(folder.get_Parent());
    var values = (cache.get(parentFoldersKey) || []);
    values.push(folder.getObjectReference());
    cache.put(parentFoldersKey, values);
}

function getRecycleBinFolder(objectStore) {
    var userPath = "/ECMP Zero Footprint Client Test/" + getUsername(objectStore) + "/Prullenbak";
    System.out.println("User Path: " + userPath );
    return Factory.Folder.fetchInstance(objectStore, userPath, null);
}

function getUsername(objectStore) {
    var connection = objectStore.getConnection();
    var filter = new PropertyFilter();
    filter.addIncludeProperty(0, null, null, PropertyNames.SHORT_NAME, null);
    var currentUser = Factory.User.fetchCurrent(connection, filter);
    return currentUser.getProperties().getStringValue(PropertyNames.SHORT_NAME);
}
```
First the method `#!js cacheFolderParent()` puts the parent folder of the folder that is deleted in the cache in a similar way as the documents. Next this script moves the folder that is being deleted to the root folder of the recycle bin.  If the cache contains cache entries for child documents of child folders then the methods `#!js restorePreviousChildDocuments()` and `#!js restorePreviousChildFolder()` will restore the documents and the subfolders, recreating the original hierarchy.

The code also handles some special situations. If a the root folder of the recycle bin already contains a folder with the same name then moving the folder will raise an exception. Therefore the method `#!js ensureUniqueName()` will make sure that folder will get a unique name prior to moving.

There is also special code to handle teamspace folders. These folders cannot be moved outside of the teamspace hierarchy. In the method `#!js moveFolder()` the folder is first marked as a non-teamspace folder by setting the value of the property `ClbTeamspace` to `#!js null`. At the end of the method the value of the property is restored.