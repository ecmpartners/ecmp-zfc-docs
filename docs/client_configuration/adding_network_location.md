* Open de Windows Explorer en kies in het context menu van **deze PC** de optie **Een netwerklokatie toevoegen**:

![Add Network Location](img/add_network_location.png)

![Network Location Welcome](img/add_network_location_wizard_1.png)

![Custom Network](img/add_network_location_wizard_2.png)

![Network Location](img/add_network_location_wizard_3.png)

![Network Name](img/add_network_location_wizard_4.png)

![Adding Completed](img/add_network_location_wizard_5.png)

![New Network Location](img/new_network_location.png)
