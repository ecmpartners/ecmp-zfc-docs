# Advanced Customization

## Introduction

There are a lot of possibilities to configure the application using the user interface. But sometimes this is not sufficient to implement your bussines needs. In that situation you have to create a custom ECMP ZFC server based on the WAR-file shipped with the application. This section will show you how you can accomplish this task.

## Creating a custom ZFC server

To create a custom EAR-file you have to perform the following steps:

* Add the ECMP ZFC server _WAR-file_ to your Maven repository, either locally:
```
mvn org.apache.maven.plugins:maven-install-plugin:2.5.2:install-file -Dfile=ecmp-zfc-server.war
```
or remote
```
mvn org.apache.maven.plugins:maven-deploy-plugin:deploy-file -Dfile=ecmp-zfc-server.war \
 -DrepositoryId=yourrepositoryId \
 -Durl=https://nexus.example.com/repository/yourrepostiory/ \
 -DgroupId=nl.ecmpartners \
 -DartifactId=ecmp-zfc-server \
 -Dversion=1.1.0
```
* Unzip the the [sample EAR-project](https://www.example.com) and add it to you development environment.
* Deploy your custom EAR-file instead of the standard application

## Changing the URL of the application

The URL of the application can be changed in the `application.xml` file. You can specify the name in `<context-root>` node of this file:

```xml
<module>
    <web>
        <web-uri>ecmp-zfc-server.war</web-uri>
        <context-root>/</context-root>
    </web>
</module>
```
## Adding external Java libraries

The server script provides you the possibility to customize the behavior of the application using JavaScript. However, sometimes you bussines needs may be to complex to handle solley with JavaScript. You may for instance have existing Java code handling the authorization of documents. In that case a hybrid solution can be used. The server script will be used to call your Java code. In that case your Java code should be added to the application. However, in order to add your Java classes to the class path of the scripting environment you will have to create a custom EAR-file containing your Java code. This section assumes that you use Maven as your build tool.

!!! info
    An alternative way is to create a shared library in WebSphere and add it to the application

* Add _all_ the dependencies of your Java code to the `<dependencies>` section:

```xml
<dependency>
    <groupId>nl.ecmpartners</groupId>
    <artifactId>ecmp-3rd-party-demo</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
<dependency>
    <groupId>com.ibm.filenet</groupId>
    <artifactId>jace</artifactId>
    <version>[5.2.1.0,)</version>
</dependency>
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.14</version>
</dependency>
```
* Also add the dependencies to the `<modules>` section as a `<jarModule>`:

```xml
<jarModule>
    <groupId>nl.ecmpartners</groupId>
    <artifactId>ecmp-3rd-party-demo</artifactId>
    <includeInApplicationXml>true</includeInApplicationXml>
    <bundleDir>lib</bundleDir>
</jarModule>
<jarModule>
    <groupId>com.ibm.filenet</groupId>
    <artifactId>jace</artifactId>
    <includeInApplicationXml>true</includeInApplicationXml>
    <bundleDir>lib</bundleDir>
</jarModule>
<jarModule>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <includeInApplicationXml>true</includeInApplicationXml>
    <bundleDir>lib</bundleDir>
</jarModule>
```