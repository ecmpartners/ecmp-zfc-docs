## Introduction

The _ECMP ZFC Server_ is an Java Enterprise application that must be deployed on an _IBM WebSphere Server_. Is is packaged as an EAR-file containing all the necessary deployment and runtime artifacts. The latest version of this file can be requested from _ECM Partners_.

When you install the application it must be bound to the root context of your application server(`/`). The actual URL the clients will use will not use the root context of your application server as the base context. However, Windows Explorer will still make requests to the root context of your application server and also expect an answer for these requests. Therefore you have to make sure that there is not already an application present which is bound to the root context of your application server. In that case you should consider moving that application to a dedicated context.

## Installing the application

You can use the WebSphere console or your other preferred installation method to deploy the EAR-file. After the installation of the EAR-file is completed you have to attach the shared library you defined in [this step](websphere_installation.md#creating-the-shared-library) to the application and map the security roles to the special subjects. For these task you have to complete these steps:

* Login to the WebSphere console.
* Navigate to __Applications > WebSphere enterprise applications__ and open the _ZFC Server_ application.
* In the __References__ section select __Shared library references__.
* Select the checkbox before the module _ECMP ZFC WebDAV Server Application_.
* Select __Reference shared libraries__.
* In the new __Shared library Mapping__ page move the library _ZFCClientLibraries_ to the right and press the __OK__ button.
* Also press the __OK__ button in the __Shared library references__ page.
* (Don't press te __Save__ button yet)
* In the __Detail Properties__ section select __Security role to user/group mapping__.
* Check the __Authenticated__ role and select __All Authenticated in Application's Realm__ when pushing the __Map Special Subjects__ button.
* Check the __Everyone__ role and select __Everyone__ when pushing the __Map Special Subjects__ button.
* Press the __OK__ button
* __Save__ your configuration changes to the master configuration.
* Return the applications list and start the __ECMP ZFC Server__ application. 
* Check the server logs for any errors.

## Testing the application

You can test your installation by using a webbrowser to navigate to the URL you configured in the application configuration. First you will be prompted to supply credentials to login to your Content Engine Server. Next a webpage will show the structure of your Object Store(s) where you will be able to browse through the different folders and collections.

!!! note 
    For this to work your browser should not disable _Basic Authentication_. For the _Microsoft Edge_ browser you can check this by navigating to the [edge://policy/](edge://policy/) page and checking for the entry [AuthSchemes](https://docs.microsoft.com/en-us/DeployEdge/microsoft-edge-policies#authschemes).