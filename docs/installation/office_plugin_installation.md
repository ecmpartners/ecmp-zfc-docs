The _ECMP Zero Footprint Office Integration_ plug-in contains a few actions which will enable the editing of _Microsoft Office_ documents without any client components. These actions are described [here](../office_integration/getting_started.md). This section only describes the installation of the plug-in. Additional steps are necessary for the integration with [IBM Content Navigator](../office_integration/content_navigator.md) or [IBM Case Manager](../office_integration/case_manager.md).

## Installing the Plug-in

To install this plug-in you have to take the following steps:

* Download the [latest version](https://www.example.com) of the plug-in and copy the jar-file to the location on your WebSphere server where you want to store custom plug-ins.
* Navigate to the __admin__ desktop in the _IBM Content Navigator_ application.
* Select the __New Plug-in__ action in the __Plug-ins__ section.
* In the __New Plug-in__ dialog specify the path or URL of the location you used in the first step and __Load__ the plug-in.    
* Now the dialog will be populated with the configuration options. Supply the configuration (see the next section) and select the __Save and Close__ action.

## Configuring the Plug-in

In this section you can configure how the path the _Microfost Office_ applications should use to access the different WebDAV resources will be composed. 

!!! note 
    The configuration values should correspond with the configuration of the [ECMP Zero Footprint Client Configuration](configuration_plugin_installation.md) plug-in. Check the configuration of that plug-in for the correct values.

![Office Plug-in Configuration](img/office_plugin_configuration.png)

| Input | Explanation   |
| ----- | ------------- |
| Zero Footprint Client Server URL | Specify the URL to the WebDAV application (without trailing slash!) |
| Application Path Prefix | The prefix of the application path (without a trailing slash!). This value is used as the top level name of the URL to the application. If you don't specify a value then this part is skipped.|
| Object Store as root node | Specify if the object store is shown as the root node.|
| Base the document paths on | Specify if the document paths should be based on the containment name or the document title.|

## Configuring the Desktop

Perform the following task for each desktop using the plug-in:

* Navigate to the __admin__ desktop in the _IBM Content Navigator_ application.
* Open the desktop in the __Desktops__ window and goto to the __Plug-ins__ section of the desktop.
* If the __Enable all deployed plug-ins for use with this desktop__ radio button is checked then no further action is necessary. __Close__ the desktop.
* If the __Select the deployed plug-ins to enable for use with this desktop__ radio button is checked then you have to explicitly select the plug-in. 
* Select the __ECMP Zero Footprint Office__ plug-in.
* Finally save the desktop changes with the __Save and Close__ action.

The actions are now available to use with [IBM Content Navigator](../office_integration/content_navigator.md) or [IBM Case Manager](../office_integration/case_manager.md).