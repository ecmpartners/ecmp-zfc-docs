# WebSphere Configuration

## Specifying the application configuration location

The configuration of the application is stored in the configuration database of IBM Content Navigator. For the application to fetch this information some bootstrap properties must be set. The properties are saved as naming properties in WebSphere. To set these properties you have to complete the following steps:

* Obtain the data source name and the schema of the IBM Content Navigator database.
* Login to the WebSphere console.
* Navigate to __Environment > Naming > Name space bindings__.
* Select the __cell__ as the scope and select __New__.
* In the first step of the wizard you have to provide a binding type, select __String__.
* In the second page you have to specify the basic properties. Use the following property values:

| Input | Value  |
| ------|--------|
| Binding identifier| ecmpWebDAVServerDataSource |
| Name in name space relative to lookup name prefix 'cell/persistent/'|ecmpWebDAVServer/dataSource |
| String value| The name of the data source used by IBM Content Navigator, e.g. `ECMClientDS` |

* In the final page review and save your changes.
* Create another __String__ value with the following property values:

| Input | Value  |
| ------|--------|
| Binding identifier| ecmpWebDAVServerSchema |
| Name in name space relative to lookup name prefix 'cell/persistent/'|ecmpWebDAVServer/schema |
| String value | The name of the schema used by IBM Content Navigator, e.g. `NEXUS` |

* __Save__ your workspace changes to the master configuration.

!!! note
    A server restart is __not__ necessary.

## Creating the shared library

The application uses the Content Engine client library to communicate with the Content Engine. The Content Engine client library must match the Content Engine server version you are using. Therefore the library is not shipped with the _ECMP ZFC Server_ but provided externally by a shared library.

Before you can start you have to obtain the location of the Content Engine client library. You can use these two methods to do this:

 * Obtain the location of the folder containing the file `Jace.jar` of the Content Engine client library on your WebSphere server. This folder is usually a folder (or a subfolder of the folder) named `CE_API`. For example you Content Navigator installation will contain a folder called `configure/CE_API` containing the client library files.
 * Another way to obtain the client libraries is to start the __IBM Administrative Console for Content Platform Engine__ and navigate to __Client API Download > IBM FileNet Content Manager > Java CEWS client__. Download the zipfile and unzip the file to a location on the WebSphere server.

!!! tip
    You can check if the version number in the file `META-INF/MANIFEST.MF` contained in the file `Jace.jar` matches your Content Engine Server version. You can determine the server version by using the http://ceserver:port/FileNet/Engine web page.

Now you can create the shared library using the following steps:

* Login to the WebSphere console.
* Navigate to __Environment > Shared Libraries__.
* Select the __server__ as the scope and select __New__.
* In the new page you have to specify the shared library properties. Use the following property values:

| Input | Value  |
| ------|--------|
| Name| ZFCClientLibraries |
| Description |The Content Engine Client Libraries and additional libraries|
| Classpath| The path of the folder containing the `Jace.jar` file, e.g. `/opt/IBM/ECMClient/configure/CE_API` |

* Select __Apply__ to apply your changes.
* __Save__ your workspace changes to the master configuration.

!!! note
    A server restart is __not__ necessary. You have to restart the server if you make subsequent changes to the shared library.

## Enabling application security

The application leverages _Application Security_ for the authentication of users. Therefore you have to make sure that this is turned on. You can check this with the following steps:

* Login to the WebSphere console.
* Navigate to __Security > Global security__.
* Make sure that __Enable application security__ is checked in the __Application security__ section. 
* Change this if this is not the case and __Save__ your workspace changes to the master configuration.

## Configuring an HTTPS connection

Without changes to the Windows registry, connecting clients to the application will not work if SSL is not properly configured. There are two ways to accomplish an HTTPS connection:

* You can put a load balancer in front of your WebSphere server which will handle the HTTPS connection. If the load balancer uses SSL offloading then you have to [disable SSL](configuration_plugin_installation.md) in the __Advanced Configuration__ section of the application configuration.
* You can let WebSphere handle the HTTPS connection. In that case you have to obtain a certificate from your certificate provider. The domain of the certificate must match the DNS name of your WebSphere server. Note that self-signed certificates will only work if you have the rights to add the self-signed certificate to the trusted certificates store of the client machines.

You have to perform these steps to import the certificate:

* Copy the keystore file of the certificate the application server.
* First go to __Security > SSL certificate and key management__.
* In the __Related Items__ section select  __Key stores and certificates__.
* Select __NodeDefaultKeyStore__ and in the next page in the __Additional Properties__ section __Personal certificates__.
* Select the __Import…__ action
* Provide the name of the keystore file you copied in the first step.
* Keep the value PKCS12 for the type field and supply the password of the keystore file.
* Press the __Get Key File Aliases__ button. This will populate the __Certificate alias to import__ list. Select your domain and save your changes.

The next steps is to set this certificate as the default for the SSL connections:

* First go to __Security > SSL certificate and key management__.
* In the __Related Items__ section select __SSL configurations__ and select __NodeDefaultSSLSettings__ in the next page.
* Select your domain in the lists __Default server certificate alias__ and __Default client certificate alias__.
* Save your changes.
* Restart your server

!!! success
    Now you are ready to deploy the _ECMP ZFC Server_ application.