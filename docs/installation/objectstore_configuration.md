# Object Store Configuration

The configuration of the application is stored in a folder structure in the Object Store. This step will add the folders and document classes necessary for the application configuration. Following standard IBM conventions the classes and folders are distributed as Object Store Add-ons. The Add-on must first be installed on the domain level and should next be deployed in each Object Store you want to use with the application.

## Install the Zero Footprint Client Object Store add-on

To install the Add-on you have to take the following steps:

* Download the Add-on file [ECMPZeroFootprintExtension.zip](../download/head/ECMPZeroFootprintExtension-1.0.0.zip) and unzip it on your computer.

* Start the __IBM Administrative Console for Content Platform Engine__ and navigate to __Global Configuration > Data Design > Add-ons__.

* Create a __New__ Add-on. In the first screen you can choose either the descriptor method or the manual method. 

!!! note
    Older versions of the Content Engine will not show this first screen and only support the manual method.

* If you choose the _descriptor_ method perform the following steps:

    * On your local file system navigate to the place where you unzipped the add-on and select the descriptor file _1.0.0ECMPZeroFootprintExtension.desc_.
    * In the next page enter the following values:
    * Import data set: Browse to the file <tt>1.0.0ECMPZeroFootprintExtension.xml</tt>
    * Post-import scrip: Browse to the file <tt>1.0.0ECMPZeroFootprintExtension_ImportScript.js</tt>
    * Review the input on the next page, select __Finish__ to create the Add-on.

* If you choose the _manual_ method perform the following steps:

    * In the next page enter the following values:
    * Display name: _1.0.0 ECMP Zero Footprint Client Extension_
    * Description: _Installs PropertyTemplates and ClassDefinitions required by the ECMP Zero Footprint Client._
    * Type: _Optional_
    * Import data set: Browse to the file <tt>1.0.0ECMPZeroFootprintExtension.xml</tt>
    * Post-import scrip: Browse to the file <tt>1.0.0ECMPZeroFootprintExtension_ImportScript.js</tt>
    * Make no changes on the next page
    * Review the input on the next page, select __Finish__ to create the Add-on.

## Deploy the Add-on in the Object Store

To deploy the Add-on in the Object Store you have to take the following steps:

* Select __Install Add-on Features__ from the context menu of the Object Store or from the __Actions__ menu of the properties page.
* Check the __1.0.0 ECMP Zero Footprint Client Extension__ extension in the top list.
* Select the __OK__ button to start the installation.

## Verify the installation

You can verify the installation by inspecting the Object Store. The Add-on should have created the following artifacts:

* The document classes _Class Configuration_ (`EcmpClassConfiguration`), _Collection Configuration_ (`EcmpCollectionConfiguration`) and _Server Script_ (`EcmpServerScript`). The display name may vary based on the locale. These are hidden document classes, they will not be visible to normal users.

* The following hidden folder structure:
```
/ECMP Zero Footprint Client
        /Class Configuration
        /Collections
        /Server Scripts
```        