# Getting Started

The _ECMP Zero Footprint_ application contains of several components which must be installed in your _IBM FileNet_ environment. This section contains the different task you should perform to complete the installation. Each task corresponds to one of the components depicted in the [architecture diagram](../baseline_architecture.md).

* First you have to [configure the object store](./objectstore_configuration.md). This will create some hidden document classes and folders in the object store.

* This next step is optional, based on the way you want to deploy the system. You have to install the [ECMP Zero Footprint Client Configuration](./configuration_plugin_installation.md) plug-in on at least one system. This will produce the necessary [configuration artifacts](configuration_artifacts.md) for the application. You can then use your deployment tooling to propagate these artifacts to other environments.

* In the next step you have to [configure WebSphere](./websphere_installation.md). This will add some bootstrap properties the _ZFC Server_ application will use to access the Content Navigator database.

* In the next step you have to install the [ZFC Server](zfc_server_installation.md) application. This is the main application that will handle all the request from the clients.

* The final step is optional. If you want to use the _ECMP Zero Footprint Client_ application to create or edit Microsoft Office documents from _IBM Content Navigator_ or _IBM Case Manager_ then you have to install and configure the [ECMP Zero Footprint Office Integration](./office_plugin_installation.md) plug-in. If you want to create or edit Microsoft Office documents from a custom web application then this plug-in is not necessary. In that case follow the instructions given [here](../office_integration/custom_web_application.md)