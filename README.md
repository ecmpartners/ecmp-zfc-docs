# Overview

This repository contains the documentation for the ECMP Zero Footprint Client. It is based on [mkdocs](https://www.mkdocs.org) combined with the [mkdocs-material](https://squidfunk.github.io/mkdocs-material) extension. You have to install both of these projects (and python) to show this documentation localy. If you have these installed use this command to start the local webserver:

```
mkdocs serve
```

# WebSphere deployment

The documentation can be deployed to WebSphere as a war-file. You need Maven to do this. Use these command to generate the war-file:
```
mkdocs build
mvn package
```
The generated war-file can be found in the `target` folder.

# Netlify deployment

The documentation can be deployed to [Netlify](https://www.netlify.com). There a couple of files which are necessary for this deployment:
* `runtime.txt` contains the required python version. Make sure that this is also a version currently supported by Netlify.
* `netlify.toml` contains the build command for this project.
* `Pipfile` describes the python dependencies. It contains the version of mkdocs and an mkdocs-material.
* `requirements.txt` contains the python packages used by the documentation. This file can be partially generated with the following command:

```
pip freeze > requirements.txt
```
Next add this line to the top of the file:

```
-i https://pypi.org/simple/
```

The only thing you have to do in Netlify is to create a new web-application based on the files stored in the BitBucket repository.

When you are upgrading a component make sure that you check for possible changes in one of these files.